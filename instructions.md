# Programmation sur micro-contrôleur PIC

## Installation de l'environnement

1. Brancher le PICkit sur l'ordinateur avec le câble USB.
2. Attendre que les pilotes s'installent (ça devrait se faire automatiquement).
3. Installer l'environnement de développement (IDE), qui sera le logiciel qui permettra de créer nos programmes :

    * Double cliquer sur le fichier `MPLABX-v5.45-windows-installer.exe` et suivre les instructions.

4. Installer le compilateur, qui permettra de traduire le langage C en langage machine :

    * Double cliquer sur le fichier `xc16-v1.61-full-install-windows64-installer.exe` et suivre les instructions.

## Création d'un nouveau projet

1. Ouvrir MPLABX en mode administrateur : 

    * Cliquer droit sur **MPLAB X IDE** et choisir **Exécuter en tant qu'administrateur** si on est sur Windows.

2. Créer un nouveau projet :

    * Dans le menu *File*, choisir **New Project**.
    * Choisir dans la liste des modèles de projets le modèle .**Standalone Project** puis cliquer **Next**.
    * Dans *Family* choisir **16-bit MCUs (PIC24)**, dans *Device* choisir **PIC24FJ64GB002** (c'est la référence de notre PIC) et dans *Tool* choisir **PICkit 3**.
    * Sélectionner **XC16** puis **Next**.
    * Entrer le nom du projet, l'emplacement du projet si besoin et continuer.

    C'est bon ! Le projet est créé mais il est vide, on va lui ajouter des fichiers.
    
3. Ajouter un fichier source
    * Dans la partie *Projects*, faire un clic droit sur le dossier **Source Files**.
    * Cliquer sur *New -> Other*.
    * Dans *Categories*, sélectionner **C** (parce qu'on code en C), sélectionner à droite **C Source File** et cliquer sur **Next**.
    * Lui donner un nom et cliquer sur **Finish**.

4. Ajouter le fichier d'entête du PIC.
    * Dans la partie *Projects*, faire un clic droit sur **Header Files**.
    * Cliquer sur **Add existing Item..**.
    * Naviguer jusqu'au fichier
        - Sous Linux : `/opt/microchip/xc16/v1.61/support/PIC24F/h/p24FJ64GB002.h` 
        - Sous Windows : `C:\Program Files\Microchip\xc16\v1.61\support\PIC24F\h\p24FJ64GB002.h`   
    * Cliquer sur **Select** 

## Programmation en C

### Instructions

En langage C, chaque fin d'instruction doit avoir un **point virgule à la fin**, pour indiquer au programme que l'instruction est finie.

``` c
une certaine instruction ;
une autre instruction ;
```

Pour faciliter la compréhension du code, on peut écrire des **lignes** qui ne seront **pas prises en compte par le compilateur**, on appelle ça **les commentaires**. Un commentaire commence à partir du moment où on écrit `//` jusqu'à la fin de la ligne :
``` c
instruction;
// un commentaire

instruction; // un commentaire

// un commentaire instruction;
// Même si on a écrit une instruction après notre commentaire, ce ne sera pas pris en compte
```

Il est possible de **grouper** une suite d'instructions grâce à des accolades. On appelle un tel groupe d'instructions un **bloc de code** :

```c
// ce qui suit est un bloc de code, composé de plusieurs instructions
{
    instruction;
    instruction;
    instruction;
}
```

 

### Variables
Les variables sont la base de tout langage de programmation. Ce sont des** cases mémoires** qui permettent de stocker des valeurs et qu'on peut modifier au cours du programme.

Chaque variable est définie par son **type** de données et chaque type a une **étendue différente de valeurs** et donc des usages différents.

Voici une liste des types les plus fréquents :
* `char` qui permet d'utiliser des valeurs entières comprises entre -128 et 127.
* `int` avec des valeurs entières entre -32 767 et 32 767
* `long` avec des valeurs entières entre -2 147 483 647 et 2 147 483 647
* `float` qui permet de contenir des valeurs réelles, des nombres à virgule donc.

Pour créer une variable, il faut **d'abord la déclarer** dans notre programme pour que le compilateur nous alloue un espace mémoire adapté pour qu'on puisse utiliser cette dernière.

Pour déclarer une variable, la syntaxe est la suivante :
``` c
type_de_la_variable nom_de_la_variable ; 

// par exemple :
char led_allumee;
int nombre;
float nombre_virgule;
```

Maintenant que ces variables sont déclarées, on peut les manipuler dans notre programme pour changer leur valeur, ou lire leur valeur.

Afin de **modifier** la valeur contenue dans une variable, on utilise l'opérateur `=` qui permet de mettre la valeur à droite du `=` dans la variable à gauche :

``` c
// ici la variable n'est que déclarée, donc on ne sait pas quelle valeur elle contient
int variable; 

// maintenant la variable contient la valeur 0
variable = 0;

// ici la variable contient la valeur 5
variable = 5;

// ici on déclare une variable nouvelle_variable ET on lui assigne une valeur qui est 2, ainsi on écrase la valeur assignée inconnue
int nouvelle_variable = 2;
```

Mais **attention**, la valeur que l'on met dans une variable **doit faire partie de l'ensemble des valeurs** du type de cette variable. Si on dit qu'une variable contient des valeurs `int`, qui contient donc que des nombres entiers, on ne pourra pas mettre un nombre à virgule comme `2.3`. Dans un programme, cela se traduit comme ça :

``` c
// on déclare la variable, elle a une valeur aléatoire (ici ce sera une valeur aléatoire entière)
int variable_entiere; 

// maintenant la variable contient la valeur 0, on n'a pas d'erreur
variable_entiere = 0;

// cette instruction renvoie une erreur car la variable variable_entiere est de type int et ne peut donc que contenir des valeurs entières
variable_entiere = 5.5;
```

### Conditions

Maintenant, il serait intéressant de pouvoir exécuter certaines lignes de code sous certaines conditions, ce qui nous amène aux instructions `if`, `else` et `else if`. 

#### Condition `if`

Voici un exemple avec uniquement une condition `if` :

``` c
// on déclare une variable un_nombre et on lui attribue la valeur 0
int un_nombre = 0;

// voici notre condition, j'explique après comment ça marche
if (un_nombre > 0)
{
    // bloc de code exécuté si la condition est respectée
}

// la suite du code
```

Dans l'exemple précédent, on a écrit le mot clé `if` qui nous permet de tester une condition à l'intérieur des parenthèses qui suivent ce mot-clé. L'écritude de ce mot-clé avec une condition entre parenthèse permet de vérifier que la condition est vraie. Ici la condition serait :
> Est-ce que la valeur contenue dans la variable un_nombre est supérieure à 0 ?

Si lors de l'exécution de cette ligne de programme la réponse est *Oui*, le programme continuera son exécution dans le bloc de code qui suit, indiqué par des accolades `{ }` juste après le mot-clé.

Si je résume, la première étape est d'écrire le mot-clé `if` pour indiquer qu'on veut vérifier une condition, suivi de la condition en question entre parenthèses `( )` et enfin le bloc de code entre accolades `{ }` à exécuter dans le cas où la conditon est vérifiée.

Mais attention, il faut que la condition soit une question valide pour que le programme puisse déterminer si elle est vérifiée, donc considérée vraie ou non vérifiée et donc considérée fausse. Un objet qui peut prendre les valeurs VRAI ou FAUX est un booléen.

Voici une liste de conditions valides que l'on peut mettre après un `if` :

```c
// Pour tester si une variable contient la valeur 1
( une_variable == 1 )

// Pour tester si une variable contient une valeur différente de 1
( une_variable != 1 )

// Pour tester si une variable contient une valeur supérieure à 1
( une_variable > 1 )

// ... supérieure ou égale à 1
( une_variable >= 1 )

// ... inférieure à 1
( une_variable < 1 )

// ... inférieure ou égale à 1
( une_variable <= 1 )

// Pour tester si une variable est plus petite qu'une autre
( une_variable < une_autre_variable )
```

Chacun de ces exemples vaut la valeur VRAI ou la valeur FAUX en fonction de la valeur contenue dans la variable une_variable.

On pourra alors écrire plusieurs conditions à la suite.

``` c
// on déclare une variable un_nombre et on lui attribue la valeur 0
int un_nombre = 0;

// Si notre variable vaut 0, on exécute le bloc de code A
if (un_nombre == 0)
{ 
    // bloc de code A
}

// Si notre variable vaut 1, on exécute le bloc de code B
if (un_nombre == 1)
{ 
    // bloc de code B
}

// Si notre variable vaut 2 ou plus, on exécute le bloc de code C
if (un_nombre >= 2)
{ 
    // bloc de code C
}

// la suite du code
```

Les conditions `if` ne sont **pas exclusives**, si une condition a été testée VRAI et qu'on a exécuté son bloc de code associé avant dans le code, il est possible qu'une autre condition soit validée et qu'on exécute un autre bloc de code.

#### Condition `else`

Parfois, on aura besoin d'exécuter des instructions spécifiques si la condition d'un `if` est à la valeur FAUX. Pour cela, on utilise le mot-clé `else` juste après le bloc de code associé au `if`.

```c

// Si la condition est vraie, on exécute le bloc A mais pas B
// Sinon, on exécutera B mais pas A
if (...)
{ 
    // bloc de code A
}
else 
{
    // bloc de code B
}

// dans tous les cas, on arrive ici
```

Bien sûr, on peut imbriquer des conditions dans un bloc de code associé à une autre condition :

```c

// Première condition
if (...)
{ 
    // bloc de code A

    // Condition testée que si la première condition était vraie
    if (...)
    {
        // bloc de code A.A
    }
    else
    {
        // bloc de code A.B
    }
}
else 
{
    // bloc de code B
}

// dans tous les cas, on arrive ici
```

Dans ce genre de cas, il faut bien faire attention à ne pas se perdre dans les `else` pour bien savoir à quel `if` ils font écho. Il faut également vérifier de correctement **fermer les blocs** de code avec des accolades fermantes `}`. Normalement, le logiciel devrait les créer en même temps que les accolades ouvrantes `{`.

#### Condition `else if`

Enfin, dans certains cas il pourra être utile de faire une suite de conditions qui si l'une est exécutée, les autres ne le seront pas. On utilisera alors les `else if`, qui sont à **mi-chemin** entre un `if` et son `else`. Les conditions sont alors **exclusives**.

```c
// Si la condition est vraie, on exécute le bloc A mais pas le bloc B ..
// .. même si la condition de B est vraie
if (...)
{ 
    // bloc de code A

    // Une fois A exécuté l'exécution reprend après le else
} 
else if (...) // Si A n'était pas vrai mais que cette condition est vraie
{
    // Bloc de code B
}
else  // Si aucune des conditions n'est vraie, on exécute le bloc C
{
    // bloc de code C
}

// dans tous les cas, on arrive ici
```

On peut séparer différents traitements indépendants en fonction de certains critères.
Il faut imaginer les `else if` comme des `else` auxquels on rajoute quand-même une condition.  

On peut les traduire en langage humain par "sinon, si ...".

```c
if (nombre == 0)
{ 
    // On allume la LED 1 mais pas les autres
} 
else if (nombre >= 0)
{
    // On allume la LED 2 mais pas les autres
}
else if (nombre <= 0)
{
    // On allume la LED 3 mais pas les autre
}
```

Dans ce cas là, si `nombre` vaut 0, on allume la LED 1 mais il ne testera pas les autres conditions, car il est déjà passé dans la première.

Ici l'exemple est peu pertinent mais dans des cas où nos conditions portent sur plusieurs variables, cela devient très pratique.

### Boucles

Dans la même lancée que les conditions, il pourrait être intéressant de répéter certaines parties / blocs de code lorsqu'une condition est vérifiée.

Il existe deux types de boucles : les boucles `while` et les boucles `for` qu'on traduira par boucle *Tant que* et boucle *Pour*.

#### La boucle `while`

Les boucles *Tant que* ou boucles `while` permettent de **répéter** un bloc de code **tant qu'une condition est vérifiée**. Lorsque la condition est testée et qu'elle n'est plus vraie, on "sort" de la boucle. En d'autres termes, le bloc de code associé à la condition `while` ne s'exécute plus et le programme reprend l'exécution du code après ce bloc.

La structure d'une boucle `while` est la même que celle d'une boucle `if`, à l'exception qu'il n'y a pas de mot-clé `else` possible. 

```c
while (...) // Dans les parenthèses, on met une condition comme pour le if
{
    // Ce bloc de code sera exécuté tant que la condition entre
    // parenthèses sera vraie
}

// Si on teste la condition est qu'elle n'est pas vérifiée
// on n'exécute plus le bloc de code ci-dessus et on continue
// l'exécution ici
```

Concrètement, l'exécution se passe comme suit : 

1. On arrive sur la ligne du `while`, on teste la condition
2. Elle est vraie, on entre dans le bloc de code
3. On exécute toutes les instructions du bloc de code entre accolades
4. On retourne à la ligne du `while` pour retester la condition
5. Si c'est toujours vrai, on va à l'étape 2
6. Sinon, on continue l'exécution après le bloc de code

Une autre exemple de code :

```c
// on crée une variable avec la valeur 0
int variable = 0;

while (variable < 10) 
{
    // Cette ligne de code permet d'ajouter 1 dans notre variable
    // Donc à chaque tour de boucle, on augmente de 1 sa valeur
    variable = variable + 1;
}

// Rendu ici, notre variable vaudra 10
// car la dernière fois que la condition du while a été testée,
// la variable valait 10, qui n'est pas inférieur à 10
// donc on est sorti de la boucle
```

#### La boucle `for`

On utilisera les boucles *Pour..* quand **on sait** au moment de l'exécution **combien de fois** on veut exécuter un traitement.

La syntaxe d'une boucle `for` est la suivante :
```c
for(int i = borne_min; i < borne_max; i++)
{
    // faire quelque chose ici
}
```
Concrètement, une boucle `for` incrémente une variable i tant qu'elle ne dépasse pas une certaine valeur.

### Fonctions

Dans un langage de programmation, les fonctions sont presque aussi importantes que les variables. Elles permettent de rendre le code plus **lisible** en **encapsulant** des suites d'instructions dans des blocs qui pourront être **appelés** plus tard dans le code.   

Lors de la création d'une fonction, on peut définir des **paramètres en entrée** de la fonction qui pourront être utilisés pour changer le comportement des instructions que la fonction contient.  
Lorsqu'on appellera notre fonction, on devra fournir des valeurs à la place de ces paramètres.

Les fonctions permettent en général de fournir un résultat à partir des paramètres d'entrée. C'est pour cela qu'il faut préciser le **type de retour** d'une fonction lors de sa définition.

Une définition de fonction ressemble à ça :
```c
type_de_retour nom_de_ma_fonction(type_parametre1 parametre1, type_parametre2 parametre2)
{
    // des instructions

    return une_valeur;
}
```

Tout d'abord, une fonction doit être définie en dehors de tout autre fonction. Il faut donc faire attention à ne pas écrire la définition d'une fonction dans la fonction `main()`.

Le premier mot-clé est le **type de retour** de la fonction. Il correspond au type de données de notre résultat. On peut donc utiliser les mêmes mot-clés que pour les types de variables (`char`, `int`, `float`, etc.) en fonction de ce que l'on veut renvoyer.  
Dans le cas où on ne souhaite pas fournir de résultat, il faudra mettre le mot-clé `void` à la place, qui signifie "vide".

La deuxième information à renseigner est le **nom de la fonction**. Comme pour les noms de variables, il faut choisir un nom qui ait du sens car c'est le nom qu'on utilisera pour appeler notre fonction.

Ensuite, il faut définir entre parenthèses les **paramètres** que la fonction demandera en entrée. Pour chaque paramètre il faut fournir son type et le nom qu'on utilisera au sein de notre fonction pour le manipuler.  

> Il est important de noter que le nom d'un paramètre d'une fonction n'a de sens que dans cette même fonction.

Dans le cas où on ne souhaite pas de paramètres pour notre fonction, on mettra juste des parenthèses `()` après le nom.

Enfin, on écrit le **bloc d'instructions** que notre fonction exécutera lorsqu'elle sera appelée.  

Si on a précisé un type de retour pour notre fonction, le mot-clé permettant d'envoyer un résultat est `return`.  
À noter que ce mot-clé signifie aussi la **fin de l'exécution** du bloc d'instructions de la fonction.

Ainsi, l'appel de notre fonction sera transformé par le résultat qu'elle renvoie et on pourra stocker ce résultat dans une variable.

Voici un exemple de fonction permettant d'additionner deux entiers :

```c
int addition(int nombre1, int nombre2)
{
    // on peut créer des variables dans une fonction
    // qui n'ont d'existence que dans cette dernière
    int somme = 0;

    // on fait le calcul, maintenant somme contient l'addition des deux paramètres
    somme = nombre1 + nombre2;

    // on renvoie la valeur de la variable somme
    return somme;

    // On n'atteindra jamais cet endroit car return a mis fin à l'exécution dans la fonction
}
```

Nous venons de voir comment définir une fonction, il faut maintenant savoir comment l'appeler. Bien heureusement, c'est tout l'intérêt d'utiliser des fonctions : c'est très simple !

Il suffit d'écrire le nom de notre fonction suivi de valeurs/variables à la place des paramètres :

```c
// Si on ne prend pas en compte le type de retour
nom_fonction(1, 2);

// si la fonction a un type de retour
variable = fonction_avec_retour(1, 2)

// fonction sans paramètre
variable = fonction_sans_param();

// On appelle une seconde fois cette fonction
variable2 = fonction_sans_param();
```

On peut alors remarquer plusieurs intérêts :

* Au niveau de l'appel, les traitements contenus dans la fonction sont cachés, on peut oublier la complexité des instructions et se concentrer seulement sur le résultat
* On peut appeler plusieurs fois une fonction sans avoir à ré-écrire tout le code, c'est un gain de temps énorme

## Programmation en C sur PIC

