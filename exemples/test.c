#include "p24FJ64GB002.h"
#include "xc.h"

#define    FCY    2000000UL
#include <libpic30.h>

#pragma config FWDTEN = OFF

main() 
{
    TRISA = 0;
    TRISB = 0;
    
    while (1 == 1) 
    {
        LATA = 0xFFFF;
        LATB = 0xFFFF;
        __delay_ms(1000);
        
        LATA = 0;
        LATB = 0;
        __delay_ms(1000);
    }
}