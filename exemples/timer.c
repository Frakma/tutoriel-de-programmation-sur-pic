/*
 * File:   timer.c
 * Author: leope
 *
 * Created on 27 d�cembre 2020, 02:52
 */

#include <p24FJ64GB002.h>

#define DELAY 8000

int main(void) {
    TRISB = 0;
    T1CON = 0b1000000000110000;

    TMR1 = 0;
    //Clear contents of the timer register
    
    while(1)
    {
        TMR1 = 0;
        LATB = 1;
        while(TMR1 < DELAY)
        {
            
        }
        TMR1 = 0;
        LATB = 0;
        while(TMR1 < DELAY)
        {
            
        }
    }
}
