#include <p24FJ64GB002.h>
#include "xc.h"

#define    FCY    8000000UL

#include <libpic30.h>

main()
{
    /* CONFIGURATION ADC */
    // pour mettre toutes les pins en sortie
    TRISB = 0;
    
    // Pour mettre toutes les pins en analogique
    AD1PCFG = 0;
    
    // pour le sampling manuel
    AD1CON1 = 0;
    
    // pour définir le pin qui sera l'entrée -> AN9 dans ce cas là
    AD1CHS = 9;

    AD1CSSL = 0;
    AD1CON2 = 0;
    AD1CON3 = 2;
    
    // pour lancer le mode ADC
    AD1CON1bits.ADON = 1;
    /* FIN DE CONFIGURATION ADC */
    
    /* CONFIGURATION PWM */
    
    LATBbits.LATB7 = 0;
    
    RPOR3bits.RP7R = 18;
    
    OC1TMR = 0;
    
    OC1CON1 = 0; /* It is a good practice to clear off the control bits initially */
    OC1CON2 = 0;
    OC1CON1bits.OCTSEL = 0x07;/* This selects the peripheral clock as the clock input to the OC module */
    
    OC1CON2bits.SYNCSEL = 0x1F; /* This selects the synchronization source as itself */
    OC1CON1bits.OCM = 6; /* This selects and starts the Edge Aligned PWM mode */
    
    OC1R = 60000; /* This is just a typical number, user must calculate based on the waveform requirements and the system clock */
    OC1RS = 80000; /* Determines the Period */
    
    /* FIN DE CONFIGURATION PWM */
    
    while(1)
    {
        //OC1R = lirePotentiometre();
    }
}

/**
 * Fonction qui permet de lire la valeur analogique du potentiomètre
 * @return la valeur
 */
int lirePotentiometre()
{
    AD1CON1bits.SAMP = 1;
    __delay32(3);
    AD1CON1bits.SAMP = 0;

    while(AD1CON1bits.DONE == 0);

    return ADC1BUF0;
}
