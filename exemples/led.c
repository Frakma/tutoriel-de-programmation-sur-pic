#include <p24FJ64GB002.h>

#pragma config FWDTEN = OFF

main() 
{
    // POUR PUTAIN DE METTRE LES SALOPES DE PIN EN PUTAIN DE DIGITAL
    AD1PCFG = 0xffff;

    TRISB = 0;
    
    // 0 pour output et 1 pour input
    TRISBbits.TRISB0 = 0;
    TRISBbits.TRISB1 = 1;
    TRISBbits.TRISB2 = 0;
    TRISBbits.TRISB3 = 1;
    
    // mettre � 0 les ports de sortie
    LATB = 0;
    
    // variable qui permet de savoir si on a fait le changement de valeur de la led
    char changement_led_1 = 0;
    
        // variable qui permet de savoir si on a fait le changement de valeur de la led
    char changement_led_2 = 0;
    
    // boucle infinie car 1 == 1 est toujours vrai
    while (1 == 1) 
    {
        // SI on est en train d'appuyer sur le bouton 1
        if (PORTBbits.RB1 == 1)
        {
            // SI on a pas d�j� fait le changement d'�tat de la led 1
            if (changement_led_1 != 1)
            {
                // on fait le changement de la led
                LATBbits.LATB2 = LATBbits.LATB2 ^ 1;
                
                changement_led_1 = 1;
            }
        }
        // SINON on r�initialise le changement sur la led � 1
        else
        {
            changement_led_1 = 0;
        }
        
        // SI on est en train d'appuyer sur le bouton 2
        if (PORTBbits.RB3 == 1)
        {
            // SI on a pas d�j� fait le changement d'�tat de la led 1
            if (changement_led_2 != 1)
            {
                // on fait le changement de la led
                LATBbits.LATB2 = LATBbits.LATB2 ^ 1;
                
                changement_led_2 = 1;
            }
        }
        // SINON on r�initialise le changement sur la led � 1
        else
        {
            changement_led_2 = 0;
        }
    }
}