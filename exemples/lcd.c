#include <stdio.h>

// Note : les mots binaires sont codés ici du bit le plus faible à gauche vers le plus fort à droite

const short CONFIG_NUMBER = 56;
const short INIT_NUMBER = 15;
const short ENTRYMODE_NUMBER = 6;

/**
 * Convertit un caractère en mot binaire de 8 bits
 * ASCII
 */
char* caractereVersMotBinaire(char c)
{
    static char binaire[8];

    int i;
    for(i=0; i<8 ; i++)
    {
        binaire[i]=c%2;      
        c=c/2;      
    }
    return binaire;
}

/**
 * Convertit un nombre en mot binaire de 8 bits
 * ASCII
 */
char* nombreVersMotBinaire(short nombre)
{
    static char binaire[8];

    int i;
    for(i=0; i<8 ; i++)
    {
        binaire[i]=nombre%2;      
        nombre=nombre/2;      
    }
    return binaire;
}

void modeEnable(short booleen)
{
    if (booleen != 0)
        printf("ENABLE");
    else
        printf("DISABLE");

    printf("\n");
}

void modeDonnees(short booleen)
{
    if (booleen != 0)
        printf("DONNEES");
    else
        printf("COMMANDES");

    printf("\n");
}

/**
 * Affiche dans la console un mot binaire
 * de 8 bits
 * 
 * On pourra modifier cette fonction pour envoyer un mot binaire au pic en deux fois
 */
void envoyerBinaire(char * binaire)
{
    modeEnable(1);

    int i;
    for(i=0; i<8; i++)
    {
        printf("%d",(int) binaire[i]);
    }
    printf("\n");

    modeEnable(0);
}

void initialiserLCD()
{
    modeDonnees(0);

    envoyerBinaire(nombreVersMotBinaire(CONFIG_NUMBER));
    envoyerBinaire(nombreVersMotBinaire(INIT_NUMBER));
    envoyerBinaire(nombreVersMotBinaire(ENTRYMODE_NUMBER));
}

/**
 * Affiche dans la console la traduction d'une phrase
 * en caractères binaires ASCII
 */
void afficherPhrase(char * phrase)
{
    modeDonnees(1);

    char * caracteres = phrase;
    while (*caracteres != '\0')
    {
        char* binaire = caractereVersMotBinaire(*caracteres);

        //TODO enlever, juste pour tester
        printf("%c -> ",*caracteres);

        envoyerBinaire(binaire);

        caracteres++;
    }

    modeDonnees(0);
}

int main()
{
    initialiserLCD();

    char * phrase = "PECAULT TROP FORTS";

    afficherPhrase(phrase);

    return 0;
}