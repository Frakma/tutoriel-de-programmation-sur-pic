#include <p24FJ64GB002.h>
#include "xc.h"

#define    FCY    8000000UL

#include <libpic30.h>

main()
{
    /* CONFIGURATION TIMER */
    
    int delai;
    int multiplicateur_delai = 30;
    
    T1CON = 0b1000000000110000;
    
    /* FIN DE CONFIGURATION TIMER */

    /* CONFIGURATION ADC */
    // pour mettre toutes les pins en sortie
    TRISB = 0;
    
    // Pour mettre toutes les pins en analogique
    AD1PCFG = 0;
    
    // pour le sampling manuel
    AD1CON1 = 0;
    
    // pour définir le pin qui sera l'entrée -> AN9 dans ce cas là
    AD1CHS = 9;

    AD1CSSL = 0;
    AD1CON2 = 0;
    AD1CON3 = 2;
    
    // pour lancer le mode ADC
    AD1CON1bits.ADON = 1;
    /* FIN DE PARTIE ADC */
    
    while(1)
    {
        delai = lirePotentiometre();
        
        TMR1 = 0;
        LATBbits.LATB2 = 1;
        while(TMR1 < delai * multiplicateur_delai)
        {
            delai = lirePotentiometre();
        }
        TMR1 = 0;
        LATBbits.LATB2 = 0;
        while(TMR1 < delai * multiplicateur_delai)
        {
            delai = lirePotentiometre();
        }
    }
}

/**
 * Fonction qui permet de lire la valeur analogique du potentiomètre
 * @return la valeur
 */
int lirePotentiometre()
{
    AD1CON1bits.SAMP = 1;
    __delay32(3);
    AD1CON1bits.SAMP = 0;

    
    while(AD1CON1bits.DONE == 0);

    return ADC1BUF0;
}
