#include "p24FJ64GB002.h"

#define    FCY    2000000UL
#include <libpic30.h>


// PIC24FJ64GB002 Configuration Bit Settings

// 'C' source line config statements

// CONFIG4
#pragma config DSWDTPS = DSWDTPSF       // DSWDT Postscale Select (1:2,147,483,648 (25.7 days))
#pragma config DSWDTOSC = LPRC          // Deep Sleep Watchdog Timer Oscillator Select (DSWDT uses Low Power RC Oscillator (LPRC))
#pragma config RTCOSC = SOSC            // RTCC Reference Oscillator  Select (RTCC uses Secondary Oscillator (SOSC))
#pragma config DSBOREN = ON             // Deep Sleep BOR Enable bit (BOR enabled in Deep Sleep)
#pragma config DSWDTEN = OFF            // Deep Sleep Watchdog Timer (DSWDT disabled)

// CONFIG3
#pragma config WPFP = WPFP63            // Write Protection Flash Page Segment Boundary (Highest Page (same as page 42))
#pragma config SOSCSEL = IO             // Secondary Oscillator Pin Mode Select (SOSC pins have digital I/O functions (RA4, RB4))
#pragma config WUTSEL = LEG             // Voltage Regulator Wake-up Time Select (Default regulator start-up time used)
#pragma config WPDIS = WPDIS            // Segment Write Protection Disable (Segmented code protection disabled)
#pragma config WPCFG = WPCFGDIS         // Write Protect Configuration Page Select (Last page and Flash Configuration words are unprotected)
#pragma config WPEND = WPENDMEM         // Segment Write Protection End Page Select (Write Protect from WPFP to the last page of memory)

// CONFIG2
#pragma config POSCMOD = NONE           // Primary Oscillator Select (Primary Oscillator disabled)
#pragma config I2C1SEL = PRI            // I2C1 Pin Select bit (Use default SCL1/SDA1 pins for I2C1 )
#pragma config IOL1WAY = ON             // IOLOCK One-Way Set Enable (Once set, the IOLOCK bit cannot be cleared)
#pragma config OSCIOFNC = ON            // OSCO Pin Configuration (OSCO pin functions as port I/O (RA3))
#pragma config FCKSM = CSDCMD           // Clock Switching and Fail-Safe Clock Monitor (Sw Disabled, Mon Disabled)
#pragma config FNOSC = FRCDIV           // Initial Oscillator Select (Fast RC Oscillator with Postscaler (FRCDIV))
#pragma config PLL96MHZ = ON            // 96MHz PLL Startup Select (96 MHz PLL Startup is enabled automatically on start-up)
#pragma config PLLDIV = DIV12           // USB 96 MHz PLL Prescaler Select (Oscillator input divided by 12 (48 MHz input))
#pragma config IESO = ON                // Internal External Switchover (IESO mode (Two-Speed Start-up) enabled)

// CONFIG1
#pragma config WDTPS = PS32768          // Watchdog Timer Postscaler (1:32,768)
#pragma config FWPSA = PR128            // WDT Prescaler (Prescaler ratio of 1:128)
#pragma config WINDIS = OFF             // Windowed WDT (Standard Watchdog Timer enabled,(Windowed-mode is disabled))
#pragma config FWDTEN = OFF             // Watchdog Timer (Watchdog Timer is disabled)
#pragma config ICS = PGx1               // Emulator Pin Placement Select bits (Emulator functions are shared with PGEC1/PGED1)
#pragma config GWRP = OFF               // General Segment Write Protect (Writes to program memory are allowed)
#pragma config GCP = OFF                // General Segment Code Protect (Code protection is disabled)
#pragma config JTAGEN = OFF             // JTAG Port Enable (JTAG port is disabled)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

#include <xc.h>

#define CLEAR 0b00000001
#define FOUR_BITS 0b00000010
#define FUNCTION_SET 0b00101000
#define DISPLAY 0b00001110
#define ENTRYMODE_NUMBER 0b00000110
#define RETURN_HOME 0b00000010

#define CURSOR_GAUCHE 0b00010000
#define CURSOR_DROITE 0b00010100
#define SHIFT_GAUCHE 0b00011000

#define RS LATBbits.LATB11
#define E  LATBbits.LATB10
#define D4 LATAbits.LATA2
#define D5 LATAbits.LATA3
#define D6 LATBbits.LATB2
#define D7 LATBbits.LATB3

#define B_GAUCHE PORTBbits.RB14
#define B_DROITE PORTBbits.RB13
#define PHRASE PORTBbits.RB8
#define DECAL PORTBbits.RB9
#define RESET PORTBbits.RB7

void modeEnable(short booleen)
{
    __delay_ms(1);
    
    if (booleen == 0)
        E = 0;
    else
        E = 1;
    
    __delay_ms(1);
}

void modeDonnees(short booleen)
{
    if (booleen == 0)
        RS = 0;
    else
        RS = 1;
}

/**
 * Affiche dans la console un mot binaire
 * de 8 bits
 * 
 * On pourra modifier cette fonction pour envoyer un mot binaire au pic en deux fois
 */
void envoyerBinaire4(char binaire)
{
    // La premi�re salve de bits
    modeEnable(1);

    D4 = (binaire & (1 << 4)) > 0;
    D5 = (binaire & (1 << 5)) > 0;
    D6 = (binaire & (1 << 6)) > 0;
    D7 = (binaire & (1 << 7)) > 0;
                
    modeEnable(0);
    
    // La deuxi�me salve de bits
    modeEnable(1);
    
    D4 = (binaire & (1 << 0)) > 0;
    D5 = (binaire & (1 << 1)) > 0;
    D6 = (binaire & (1 << 2)) > 0;
    D7 = (binaire & (1 << 3)) > 0;
    
    modeEnable(0);
}

void envoyerBinaire8(char binaire)
{
      // La premi�re salve de bits
    modeEnable(1);

    D4 = (binaire & (1 << 0)) > 0;
    D5 = (binaire & (1 << 1)) > 0;
    D6 = (binaire & (1 << 2)) > 0;
    D7 = (binaire & (1 << 3)) > 0;
                
    modeEnable(0);
}


void placerCurseur(char ligne, char colonne)
{
    int position = 64 * ligne + colonne;
    
    envoyerBinaire4(position | 1 << 7);
}

void initialiserLCD()
{
    modeDonnees(0);

    envoyerBinaire8(FOUR_BITS);
    
    envoyerBinaire4(CLEAR);
    envoyerBinaire4(RETURN_HOME);
    envoyerBinaire4(FUNCTION_SET);
    envoyerBinaire4(DISPLAY);
    envoyerBinaire4(ENTRYMODE_NUMBER);
}

/**
 * Affiche dans la console la traduction d'une phrase
 * en caract�res binaires ASCII
 */
void afficherPhrase(char * phrase)
{
    modeDonnees(1);

    char * caractereActuel = phrase;
    while (*caractereActuel != '\0')
    {
        __delay_ms(100);
        envoyerBinaire4(*caractereActuel);
        caractereActuel++;
    }

    modeDonnees(0);
}


/**
 * Fonction qui permet de lire la valeur analogique du potentiom�tre
 * @return la valeur
 */
int lirePotentiometre()
{
    AD1CON1bits.SAMP = 1;
    __delay32(3);
    AD1CON1bits.SAMP = 0;

    
    while(AD1CON1bits.DONE == 0);

    return ADC1BUF0;
}


void initialiserPIC()
{
    AD1PCFG = 0b1111110111111111;
    AD1CON1 = 0;
    
    // pour d�finir le pin qui sera l'entr�e -> AN9 dans ce cas l�
    AD1CHS = 9;

    AD1CSSL = 0;
    AD1CON2 = 0;
    AD1CON3 = 2;
    
    // pour lancer le mode ADC
    AD1CON1bits.ADON = 1;
    /* FIN DE PARTIE ADC */

    I2C1CONbits.I2CEN = 0;
    
    // On met tout en mode de sortie
    TRISA = 0;
    TRISB = 0;
    TRISBbits.TRISB15 = 1;
    TRISBbits.TRISB14 = 1;
    TRISBbits.TRISB13 = 1;
    TRISBbits.TRISB9 = 1;
    TRISBbits.TRISB8 = 1;
    TRISBbits.TRISB7 = 1;
    LATA = 0;
    LATB = 0;

}

int main()
{
    initialiserPIC();
    initialiserLCD();
    
    /*
    afficherPhrase("Coucou Papa");
    __delay_ms(100);
    placerCurseur(0, 7);
    afficherPhrase("Leo");
    */
    
    while(1)
    {
        unsigned char caractere =  (lirePotentiometre() / (float)1023) * ('_' - 'A') + 'A';
        modeDonnees(1);
        envoyerBinaire4(caractere);
        modeDonnees(0);
        envoyerBinaire4(CURSOR_GAUCHE);
        
        if(PHRASE)
        {
            afficherPhrase("Coucou Nathalie ");
        }
        if(DECAL)
        {
            envoyerBinaire4(SHIFT_GAUCHE);
        }
        if(RESET)
        {
            envoyerBinaire4(CLEAR);
            envoyerBinaire4(RETURN_HOME);
        }
        if(B_GAUCHE)
        {
            envoyerBinaire4(CURSOR_GAUCHE);
        }
        else if(B_DROITE)
        {
            envoyerBinaire4(CURSOR_DROITE);
        }
        __delay_ms(100);
    }
    
    return 0;
}
