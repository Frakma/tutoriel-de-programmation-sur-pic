#include "xc.h"
#include <libpic30.h>
#include <math.h>

#define FCY 8000000UL

_CONFIG1( FWDTEN_OFF  & ICS_PGx1 & GWRP_OFF & GCP_OFF & GWRP_OFF & JTAGEN_OFF) 
_CONFIG2( POSCMOD_NONE  & I2C1SEL_PRI & IOL1WAY_ON & OSCIOFNC_ON & FCKSM_CSDCMD  & FNOSC_FRCDIV & PLL96MHZ_OFF & IESO_OFF)
_CONFIG3(SOSCSEL_IO)



void PWMInit()
{
     /* CONFIGURATION PWM */

    RPOR3bits.RP7R = 18;
    
    OC1CON1 = 0;
    OC1CON2 = 0;
    
    OC1R = 3000-1;
    OC1RS = 40000-1;
    
    OC1CON2bits.SYNCSEL = 0x1F; //0b00011111
    OC1CON1bits.OCTSEL = 0x07;
    OC1CON1bits.OCM = 6;
    
}

void ADInit()
{
    /* CONFIGURATION ADC */
    // pour mettre toutes les pins en sortie
    TRISB = 0;
    TRISBbits.TRISB15 = 1;
    // Pour mettre toutes les pins en analogique
    AD1PCFG = 0;
    
    // pour le sampling manuel
    AD1CON1 = 0;
    
    // pour d�finir le pin qui sera l'entr�e -> AN9 dans ce cas l�
    AD1CHS = 9;

    AD1CSSL = 0;
    AD1CON2 = 0;
    AD1CON3 = 2;
    
    // pour lancer le mode ADC
    AD1CON1bits.ADON = 1;
    /* FIN DE PARTIE ADC */
}


/**
 * Fonction qui permet de lire la valeur analogique du potentiom�tre
 * @return la valeur
 */
int lirePotentiometre()
{
    AD1CON1bits.SAMP = 1;
    __delay32(3);
    AD1CON1bits.SAMP = 0;

    while(AD1CON1bits.DONE == 0);

    return ADC1BUF0;
}

void main()
{
    
    CLKDIVbits.RCDIV = 0b001; // Ratio
    PWMInit();
    ADInit();
   
    
    while(1)
    {
        
        int p = lirePotentiometre();
        p = ceil((float)p*1.95) + 2000;
        OC1R = p;
        
    }
}
